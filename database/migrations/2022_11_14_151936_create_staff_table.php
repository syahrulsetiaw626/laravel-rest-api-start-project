<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('nik');
            $table->string('id_number');
            $table->string('phone');
            $table->string('place_of_birth');
            $table->datetime('date_of_birth');
            $table->string('address');
            $table->string('photo')->nullable();
            $table->string('rekening_number')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('position_id')->nullable();
            $table->integer('type_contract_id')->nullable();
            $table->integer('salary_per_day')->nullable();
            $table->integer('salary_trainee')->nullable();
            $table->integer('main_salary')->nullable();
            $table->boolean('is_active')->default(true);
            $table->dateTime('join_date')->nullable();
            $table->dateTime('resign_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
};
