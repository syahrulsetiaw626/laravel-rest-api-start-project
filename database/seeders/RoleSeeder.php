<?php

namespace Database\Seeders;

use App\Enum\GeneralEnum;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default = ['create', 'update', 'read', 'delete'];
        $permissions = ['staff','permission', 'role'];

        $permissiones = [];
        foreach ($permissions as $key => $permission) {
          foreach ($default as $kez => $def) {
            $toPush = Permission::create(['name' => $def . " " . $permission]);
            array_push($permissiones, $toPush);
          }
        }

        $listRole = ['superadmin', 'manager', 'supervisor', 'staff'];

        $roles = [];
        foreach ($listRole as $key => $role) {
          $to = Role::create(['name' => $role]);
          array_push($roles, $to);
        }

        foreach ($permissiones as $key => $permi) {
          $roles[0]->givePermissionTo($permi);
        }

    }
}
