<?php 
  
namespace App\Service;

use App\Enum\ResponseEnum;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class PermissionService{

  public function checkToken(){
    $user = auth("api")->user();
    
    if(!$user){
      throw new UnauthorizedHttpException("-",ResponseEnum::LOGIN_FAILED->value);
    }
  }

  public function checkUserHasPermission(String $permission){
    $user = auth("api")->user();
    $allPermissions = collect($user->getAllPermissions()->pluck('name'))->toArray();
    $check = in_array($permission, $allPermissions);

    if(!$check){
      throw new AccessDeniedHttpException(ResponseEnum::ACCESS_DENIED->value);
    }
    
    return true;
  }
}