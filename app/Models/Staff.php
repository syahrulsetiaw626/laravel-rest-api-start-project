<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
      'user_id', 'nik','id_number', 'phone', 'place_of_birth', 'date_of_birth', 'address', 'photo', 'rekening_number', 'department_id', 'position_id', 'type_contract_id',
      'salary_per_day', 'salary_trainee', 'main_salary', 'is_active', 'join_date', 'resign_date'
    ];

    public function user(){
      return $this->hasOne(User::class, 'id', 'user_id');
    }
}
