<?php

namespace App\Http\Controllers\API;

use App\Enum\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Service\PermissionService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
      //checkToken
      (new PermissionService())->checkToken();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('read role');

          // create data warga
          $role = Role::paginate(10);

          $response['message'] = ResponseEnum::READ_SUCCESS->value;
          $response['data'] = $role;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::READ_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
          $request->validate([
            'name' => 'required|unique:roles,name',
            'permissions' => 'array'
          ]);

          // check permission
          (new PermissionService())->checkUserHasPermission('create role');

          // create data warga
          $role = Role::create(['name' => $request->name]);

          $role->permissions()->attach($request->permissions);

          $response['message'] = ResponseEnum::STORE_SUCCESS->value;
          $response['data'] = $role;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::STORE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('read role');

          // create data warga
          $role = Role::findOrFail($id);

          $response['message'] = ResponseEnum::READ_SUCCESS->value;
          $response['data'] = $role;
          $response['data']['permission'] = $role->permissions->pluck('id');
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::READ_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'unique:roles,name',
            'permissions' => 'array'
        ]);
        
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('update role');

          // create data warga
          $role = Role::findOrFail($id);

          if(isset($request->name)){
            $role->update(['name' => $request->name]);
          }

          if(isset($request->permissions)){
            $role->permissions()->sync($request->permissions);
          }

          $response['message'] = ResponseEnum::UPDATE_SUCCESS->value;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::UPDATE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('delete role');

          // create data warga
          $role = Role::findOrFail($id)->delete();

          $response['message'] = ResponseEnum::DELETE_SUCCESS->value;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::DELETE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }
}
