<?php

namespace App\Http\Controllers\API;

use App\Enum\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Service\PermissionService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        //checkToken
        (new PermissionService())->checkToken();
    }

    public function index(Request $request){
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('read permission');

          if($request->keyword){
            $permission = Permission::where('name', 'like', '%'.$request->keyword.'%')->paginate(10);
          }else{
            $permission = Permission::latest()->paginate(10);
          }
          

          $response['message'] = ResponseEnum::READ_SUCCESS->value;
          $response['data'] = $permission;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::READ_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    public function store(Request $request){
      try {
          $request->validate([
            'name' => 'required|unique:permissions,name'
          ]);

          // check permission
          (new PermissionService())->checkUserHasPermission('create permission');

          
          $permission = Permission::create(['name' => $request->name]);

          $response['message'] = ResponseEnum::STORE_SUCCESS->value;
          $response['data'] = $permission;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::STORE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    public function update(Request $request, $id){
      try {
          $request->validate([
            'name' => 'unique:permissions,name'
          ]);
          // check permission
          (new PermissionService())->checkUserHasPermission('update permission');

          
          $permission = Permission::findOrFail($id)->update(['name' => $request->name]);

          $response['message'] = ResponseEnum::UPDATE_SUCCESS->value;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::UPDATE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    public function destroy($id){
      try {
          // check permission
          (new PermissionService())->checkUserHasPermission('delete permission');

          
          $permission = Permission::findOrFail($id)->delete();

          $response['message'] = ResponseEnum::DELETE_SUCCESS->value;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::DELETE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }
}
