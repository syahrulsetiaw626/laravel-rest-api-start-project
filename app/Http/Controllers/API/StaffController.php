<?php

namespace App\Http\Controllers\API;

use App\Enum\GeneralEnum;
use App\Enum\ResponseEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\StaffRequest;
use App\Models\Staff;
use App\Models\User;
use App\Service\PermissionService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{
    public function __construct()
    {
        //checkToken
        (new PermissionService())->checkToken();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('read staff');

          $staff = Staff::latest()->paginate(10);
          $response['message'] = ResponseEnum::READ_SUCCESS->value;
          $response['data'] = $staff;

          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::READ_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaffRequest $request)
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('create staff');

          $input = $request->all();
          $input['password'] = Hash::make(GeneralEnum::DEFAULT_PASSWORD->value);

          // create user and assign to 'staff'
          $user = User::create($input);
          $user->assignRole('staff');
          $input['user_id'] = $user->id;

          // create data staff
          Staff::create($input);

          $response['message'] = ResponseEnum::STORE_SUCCESS->value;
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::STORE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          // check permission
          (new PermissionService())->checkUserHasPermission('read staff');

          // create data warga
          $staff = Staff::find($id);

          $response['message'] = ResponseEnum::READ_SUCCESS->value;
          $response['data'] = [
            'staff' => $staff,
            'user' => $staff->user,
            'role' => $staff->user->roles
          ];
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::READ_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
      try {
        
          $input = $request->validate([
            'name' => 'string',
            'email' => 'email|unique:users,email',
            'id_number' => 'string',
            'nik' => 'string',
            'phone' => 'string',
            'place_of_birth' => 'string',
            'date_of_birth' => 'date',
            'address' => 'string',
            'photo' => 'string',
            'rekening_number' => 'string',
            'department_id' => 'integer',
            'position_id' => 'integer',
            'type_contract_id' => 'integer',
            'salary_per_day' => 'integer',
            'salary_trainee' => 'integer',
            'main_salary' => 'integer',
            'join_date' => 'date',
            'resign_date' => 'date',
            'role' => 'array'
          ]);

          // check permission
          (new PermissionService())->checkUserHasPermission('update staff');

          // create data staff
          $staff = Staff::findOrFail($id);

          if(isset($input['resign_date'])){
            $staff->is_active = false;
            $staff->save();
          }

          $staff->update($input);

          if(isset($input['name']) || isset($input['email'])){
            $user = User::find($staff->user_id);
            $user->update($input);
          }

          if(isset($input['role'])){
            $user = User::find($staff->user_id);
            $user->syncRoles($input['role']);
          }

          $response['message'] = ResponseEnum::UPDATE_SUCCESS->value;
          
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::UPDATE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {          
          // check permission
          (new PermissionService())->checkUserHasPermission('delete staff');

          // create data staff
          $staff = staff::findOrFail($id);
          User::find($staff->user_id)->delete();
          $staff->delete();

          $response['message'] = ResponseEnum::DELETE_SUCCESS->value;
          return response()->json($response, 200);
        } catch (QueryException $e) {
          $response['message'] = ResponseEnum::DELETE_FAILED->value;
          $response['error'] = $e;
          return response()->json($response, $e->getCode());
        }
    }
}
