<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'nik' => 'required|string',
            'id_number' => 'required|string',
            'phone' => 'required|string',
            'place_of_birth' => 'required|string',
            'date_of_birth' => 'required|string',
            'address' => 'required|string',
            'photo' => 'string',
            'rekening_number' => 'required|string',
            'department_id' => 'integer',
            'position_id' => 'integer',
            'type_contract_id' => 'integer',
            'salary_per_day' => 'required|integer',
            'salary_trainee' => 'required|integer',
            'main_salary' => 'required|integer',
            'join_date' => 'date',
            'resign_date' => 'date',
        ];
    }
}
